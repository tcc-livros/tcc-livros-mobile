import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_controller.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_module.dart';

void main() {
  initModule(EditBookshelfModule());
  EditBookshelfController editbookshelf;

  setUp(() {
    editbookshelf = EditBookshelfModule.to.get<EditBookshelfController>();
  });

  group('EditBookshelfController Test', () {
    test("First Test", () {
      expect(editbookshelf, isInstanceOf<EditBookshelfController>());
    });
  });
}
