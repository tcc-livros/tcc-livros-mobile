import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';

import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_page.dart';

main() {
  testWidgets('EditBookshelfPage has title', (WidgetTester tester) async {
    await tester.pumpWidget(
        buildTestableWidget(EditBookshelfPage(title: 'EditBookshelf')));
    final titleFinder = find.text('EditBookshelf');
    expect(titleFinder, findsOneWidget);
  });
}
