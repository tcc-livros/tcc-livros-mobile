import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';


import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_controller.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_module.dart';

void main() {
  initModule(SidemenuModule());
  SidemenuController sidemenu;

  setUp(() {
    sidemenu = SidemenuModule.to.get<SidemenuController>();
  });

  group('SidemenuController Test', () {
    test("First Test", () {
      expect(sidemenu, isInstanceOf<SidemenuController>());
    });

  });
}
