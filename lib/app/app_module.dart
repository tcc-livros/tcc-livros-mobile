import 'package:dio/dio.dart';
import 'package:tcc_mobile/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/app_widget.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_module.dart';
import 'package:tcc_mobile/app/modules/home/home_module.dart';
import 'package:tcc_mobile/app/modules/login/login_module.dart';
import 'package:tcc_mobile/app/modules/register/register_module.dart';
import 'package:tcc_mobile/app/shared/auth/auth_controller.dart';
import 'package:tcc_mobile/app/shared/auth/repositories/auth_repository.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/bookshelf_repository.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/register_repository.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/register_repository_interface.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/bookshelf_controller.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/register_rep_controller.dart';
import 'package:tcc_mobile/app/shared/utils/constants.dart';
import 'shared/auth/repositories/auth_repository_interface.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => HomeModule()),
        Bind((i) => Dio(BaseOptions(baseUrl: URL_BASE))),
        Bind((i) => AuthController()),
        Bind<IAuthRepository>((i) => AuthRepository(i.get<Dio>())),
        Bind((i) => RegisterModule()),
        Bind((i) => RegisterRepositoryController()),
        Bind<IRegisterRepository>((i) => RegisterRepository(i.get<Dio>())),
        Bind((i) => BookshelfRepositoryController()),
        Bind((i)=> BookshelfRepository(i.get<Dio>())),

      ];

  @override
  List<Router> get routers => [
        Router('/', module: LoginModule()),
        Router('/home', module: HomeModule()),
        Router('/register', module: RegisterModule()),
        Router('/bookshelf', module: EditBookshelfModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
