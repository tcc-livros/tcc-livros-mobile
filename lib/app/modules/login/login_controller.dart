import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/auth/auth_controller.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _$LoginController;

abstract class _LoginBase with Store {


  @observable
  String errorMessage = ""; 

  @action
  updateErrorMessage(String value) => errorMessage = value;

  @observable
  String email;

  @action 
  changeEmail(String value) => email = value;

  @observable
  String password;

  @action
  changePassword(String value) => password = value;
  
  final AuthController auth = Modular.get();

  @action
  Future loginWithPassword() async{

    try{
      await auth.loginWithPassword(email, password);

      if(await auth.user != null){
        Modular.to.pushReplacementNamed('/home');
      }
      else if(await auth.user == null){
        print('Login invalido');
      }
    } 
    on Exception catch(error){
      print(error.toString().substring(11));
      updateErrorMessage(error.toString().substring(11));    
    }
  }

  @action
  Future logOut() async {
    auth.logOut();
    Modular.to.pushNamedAndRemoveUntil('/', ModalRoute.withName('/'));
  }
  
}
