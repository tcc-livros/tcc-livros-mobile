import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/login/login_controller.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  final _formKey = new GlobalKey<FormState>();


  bool _submitForm(){
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();

      controller.loginWithPassword();
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView( 
        children: <Widget>[
          _showBody(),
        ],
      ),
    );
  }

  Widget _showBody(){
    return new Container(
      padding: EdgeInsets.all(20.0),
      child: new Form(
        key: _formKey,
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            _showLogo(),
            _showEmailInput(),
            _showPasswordInput(),
            _showErrorMessage(),
            _showLoginButton(),
            _showSignUpButton(),
          ],
        ),
      ),
    );
  }

  Widget _showLogo(){
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 60.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('assets/images/p2plivros.png', height: 2000, width: 1500),
        ),
      ),
      );
  }

  Widget _showEmailInput(){
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 35.0, 0.0, 0.0),
      child: Observer(builder:(_){
        return TextFormField(
          maxLines: 1,
          keyboardType: TextInputType.emailAddress,
          autofocus: false,
          decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(Icons.mail, color: Color(0xffAD57A5))
          ),
          validator: (value) {
            if(value.isEmpty){
              return 'esse campo não pode estar vazio';
              }
            else if(!value.contains('@') || !value.contains('.')){
              return 'Email inválido';
            }else{return null;}
              },
          onChanged: controller.changeEmail
        );
        }
      ),
    );
  }

  Widget _showPasswordInput(){
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: Observer(
              builder: (_){
              return new TextFormField(
                  maxLines: 1,
                  obscureText: true,
                  autofocus: false,
                  decoration: new InputDecoration(
                    hintText: 'Senha',
                    icon: new Icon(Icons.lock, color: Colors.purple),
                  ),
                  validator: (value) {
                    if(value.isEmpty){
                      return 'esse campo não pode estar vazio';
                      }
                    else if(value.length < 3 ){
                      return 'Senha muito curta.';
                    }else{return null;}
                      },
                  onChanged: controller.changePassword
                );
             }
            ),
          );
  }

   Widget _showErrorMessage(){
    return Observer(builder: (_){
        if(controller.errorMessage.length > 0 && controller.errorMessage != null){
          return Padding(
                  padding: const EdgeInsets.fromLTRB(50.0, 20.0, 0.0, 0.0),
                  child: Text(
                    controller.errorMessage,
                    style: TextStyle(
                      fontSize: 13.0,
                      color: Colors.red[700],
                      height: 1.0,
                      fontWeight: FontWeight.normal),
                      ),
          );
          }
        else{
          return new Container(
            height: 0.0);
        }
      }
    );
  }

  Widget _showLoginButton(){
    return new Padding(
      padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
      child: SizedBox(
        height: 45.0,
        child: new RaisedButton(
          elevation: 5.0,
          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
          color: Colors.purple,
          child: new Text('Login',
            style: new TextStyle(fontSize: 20.0, color: Colors.white),
          ),
          onPressed: _submitForm,
          ),
      ),
      );
  }

  Widget _showSignUpButton(){
    return new FlatButton(
      onPressed: () { Modular.to.pushReplacementNamed('/register');}, //change to sign up page
      child: new Text('Criar Conta',
        style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
      );
  }

}
