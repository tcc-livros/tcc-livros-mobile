import 'package:flutter/src/widgets/framework.dart';
import 'package:tcc_mobile/app/modules/home/home_controller.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_page.dart';

class SidemenuModule extends ModuleWidget {
  @override
  List<Bind> get binds => [
        Bind((i) => SidemenuController()),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => SidemenuPage()),
      ];

  static Inject get to => Inject<SidemenuModule>.of();

  @override
  Widget get view => SidemenuPage();
}
