import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/models/sharedPref.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';

part 'sidemenu_controller.g.dart';

class SidemenuController = _SidemenuBase with _$SidemenuController;

abstract class _SidemenuBase with Store {
  
  SharedPref sharedPref = SharedPref();
 
  @action
  Future<void> loadUserInfo() async {
    try{
      user = User.fromJson(await sharedPref.read('user'));
      name = user.name;
      email = user.email;
      image = user.image;
    } catch (error){
      print(error);
    }
  }

  @observable
  String name = "";

  @observable
  String email = "";

  @observable
  String image = "";


  @observable
  User user = User();
}
