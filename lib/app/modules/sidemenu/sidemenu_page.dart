import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_controller.dart';

class SidemenuPage extends StatefulWidget {
  @override
  _SidemenuPageState createState() => _SidemenuPageState();
}

class _SidemenuPageState extends ModularState<SidemenuPage, SidemenuController> {

  @override
  void initState(){
    super.initState();
    controller.loadUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) => _showSideMenu(controller.name, controller.email, controller.image));
  }

  Widget _showSideMenu(String name, String email, String image){
    return new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.purple
              ),
              accountName: new Text(name),
              accountEmail: new Text(email),
              currentAccountPicture: new GestureDetector( 
                child: _showAvatar(image)
                ),
              ),
             new ListTile(
              title: new Text("Meu Perfil"),
              leading: new Icon(Icons.account_circle),
              onTap: (){
              }
            ),
             new ListTile(
              title: new Text("Minha Estante"),
              leading: new Icon(Icons.book),
              onTap: (){
                
              }
            ),
             new ListTile(
              title: new Text("Empréstimos"),
              leading: new Icon(Icons.mail_outline),
              onTap: () {
              }
            ),
             new ListTile(
              title: new Text("Criar clube"),
              leading: new Icon(Icons.group),
              onTap: () {
              }
            ),
            new ListTile(
              title: new Text("Editar clube"),
              leading: new Icon(Icons.create),
              onTap: () {
              }
            ),
             new ListTile(
              title: new Text("Sair"),
              leading: new Icon(Icons.power_settings_new),
              onTap: () {
              }
            ),
          ],
        ),
      );
  }

  Widget _showAvatar(String image){
    if(image == null) {
      return CircleAvatar(
            backgroundColor: Colors.white,
            radius: 50,
            child: Image.asset('assets/images/p2plivros.png'),
      );
    } else if (image.isEmpty){
        return CircleAvatar(
              backgroundColor: Colors.white,
              radius: 50,
              child: Image.asset('assets/images/p2plivros.png'),
        );
    } else {
      return CircleAvatar(
            radius: 50,
            backgroundImage: NetworkImage(image),
            );
    }

  }
}
