import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';

class RecommendationWidget extends StatelessWidget {
  final List<Bookshelf> recommendedBooks;
  
  const RecommendationWidget(
    {
      Key key,
      @required this.recommendedBooks,
    })
    : super(key: key);



  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 190,
        child: ListView.builder(  
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: recommendedBooks.length,
          itemBuilder: (context, index){
            return _showRecommendatinCard(
                    recommendedBooks[index].book.title, 
                    recommendedBooks[index].book.authors.first.name,
                    recommendedBooks[index].book.imageUrl);
          }
        ),
    );
  }

  Widget _showRecommendatinCard(String bookName, String author, String bookImage){
    return Container(
          child: Column(
           children: <Widget>[
              Container(
                height: 120,
                width: 85.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(bookImage)),
                ),
              ),
               Container(
                  child: bookName.length <=17 ? Text(
                    bookName, style: TextStyle(fontSize: 18),
                  ) :  Text(
                    bookName.substring(0,15)+'...', style: TextStyle(fontSize: 18),
                  ) 
                ),
                Container(
                  child: author.length <= 18 ? Text(
                    author, style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                  ) :  Text(
                    author.substring(0,15)+'...', style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                  ),
                ),
            ],
      ),
    );
  }
}