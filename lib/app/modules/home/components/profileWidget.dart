import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ProfileWidget extends StatelessWidget {

  final String name;
  final String email;
  final String telegram;
  final String image;
  final String booksNumber;
  final String userRating;
  final String lastSeen;
  final String bio;

  const ProfileWidget(
    {
      Key key,
      @required this.name,
      @required this.email,
      @required this.telegram,
      @required this.image,
      @required this.bio,
      @required this.booksNumber,
      @required this.userRating,
      @required this.lastSeen,
    })
    : super(key: key);



  @override
  Widget build(BuildContext context) {
      return Container(
      child: Column(
        children: <Widget>[
          _showPhotoSection(image),
          _showNameSection(name),
          _showContactSection(email, telegram),
          _showBioSection(bio),
          _showInfoSection(booksNumber, userRating, lastSeen),
        ],
      ),
    );
  }

  Widget _showPhotoSection(String image) {
    if (image == null){
    return Container(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
            CircleAvatar(
            backgroundColor: Colors.white,
            radius: 50,
            child: Image.asset('assets/images/p2plivros.png'),
          ) 
        ],
      ),
    );}
    else if (image.isEmpty){
      return Container(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
            CircleAvatar(
            backgroundColor: Colors.white,
            radius: 50,
            child: Image.asset('assets/images/p2plivros.png'),
          ) 
        ],
      ),
    );
    }
    else {
      return Container(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
           CircleAvatar(
            radius: 50,
            backgroundImage: NetworkImage(image),
            ), 
        ],
      ),
    );
    }
  }

  Widget _showNameSection(String name) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
            child: Text(
              name,
              style: new TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
  
  Widget _showContactSection(String email, String telegram) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              padding: EdgeInsets.fromLTRB(0, 5, 5, 0),
              child: Text(
                email,
                style: new TextStyle(fontSize: 18),
              )),
          telegram != null ? Container(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                '| $telegram',
                style: new TextStyle(fontSize: 18),
              )) : Container(height: 0,),
        ],
    );
  }

  Widget _showBioSection(String bio){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        bio != null  ? Container(
          padding: EdgeInsets.only(top: 5),
          child: Text(
                   bio, style: TextStyle(fontSize: 18),
                  ),
          ) : Container(height: 0,)
        ],
      );
  }



   Widget _showInfoSection(String booksNumber, String userRating, String lastSeen) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 5),
                child: Text(
                  'Estante',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                child: Text(
                  booksNumber,
                  style: TextStyle(fontSize: 18, ),
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 5),
                child: Row(
                        children:<Widget>[ 
                          Text(
                              'Avaliação',
                              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                            ),
                          Icon(Icons.star, size: 25, color: Colors.yellow[700]),
                        ],
                ),
              ),
              Container(
                child: Text(
                  userRating,
                  style: TextStyle(fontSize: 18, ),
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 5),
                child: Text(
                  'Visualizado',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                ),
              ),
              Container(
                child: lastSeen.length > 1 ? Text(
                 '${DateFormat.yMd('pt_BR').format(DateTime.parse(lastSeen))}',
                  style: TextStyle(fontSize: 18, ),
                ) : Text(lastSeen),
              ),
            ],
          ),
        ],
    );
  }

}