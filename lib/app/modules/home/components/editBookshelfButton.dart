import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class EditBookshelfButtonWidget extends StatefulWidget {
  @override
  _EditBookshelfButtonWidgetState createState() => _EditBookshelfButtonWidgetState();
}

class _EditBookshelfButtonWidgetState extends State<EditBookshelfButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return editBookShelfButton(context);
  }
  
  Widget editBookShelfButton(BuildContext context){
    return RaisedButton(
              color: Colors.green[50],
              child: Text('Editar Estante', style: TextStyle(color: Colors.black),),
              onPressed: (){
                Modular.to.pushReplacementNamed('/bookshelf');
              }
              );
  }
}