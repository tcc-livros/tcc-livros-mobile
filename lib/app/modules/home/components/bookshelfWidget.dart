import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/modules/home/components/editBookshelfButton.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';

class BookshelfWidget extends StatelessWidget {
  final List<Bookshelf> bookshelfBooks;
 
 
 const BookshelfWidget(
    {
      Key key,
      @required this.bookshelfBooks,
    })
    : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        EditBookshelfButtonWidget(),
          SizedBox(height: 15,),
           SizedBox(
          height: 380,
          child: bookshelfBooks != null || bookshelfBooks.length > 0 ? GridView.builder(
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 0,
              crossAxisSpacing: 30,
              childAspectRatio: 1,
              ),
            scrollDirection: Axis.horizontal,
            itemCount: bookshelfBooks.length,
            itemBuilder: (context, index) {
              return _showBookShelfCard(bookshelfBooks[index].book.title, 
                                        bookshelfBooks[index].book.authors.first.name,
                                        bookshelfBooks[index].book.imageUrl);

            },
            ) : Text('Estante vazia! Adicione um livro a sua estante.', style: TextStyle(fontSize: 18),),
          
          ),
      ]
    );
  }


  Widget _showBookShelfCard(String bookName, String author, String bookImage){
    return Container(
          child: Column(
           children: <Widget>[
              Container(
                height: 120,
                width: 85.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(bookImage)),
                ),
              ),
              Container(
                child: bookName.length <=17 ? Text(
                  bookName, style: TextStyle(fontSize: 18),
                ) :  Text(
                  bookName.substring(0,15)+'...', style: TextStyle(fontSize: 18),
                ) 
              ),
                Container(
                child: author.length <= 18 ? Text(
                  author, style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                ) :  Text(
                  author.substring(0,15)+'...', style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                ),
              ),
            ],
          ),
    );
  }

}