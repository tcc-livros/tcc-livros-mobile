// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeBase, Store {
  final _$userAtom = Atom(name: '_HomeBase.user');

  @override
  User get user {
    _$userAtom.context.enforceReadPolicy(_$userAtom);
    _$userAtom.reportObserved();
    return super.user;
  }

  @override
  set user(User value) {
    _$userAtom.context.conditionallyRunInAction(() {
      super.user = value;
      _$userAtom.reportChanged();
    }, _$userAtom, name: '${_$userAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_HomeBase.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_HomeBase.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$telegramAtom = Atom(name: '_HomeBase.telegram');

  @override
  String get telegram {
    _$telegramAtom.context.enforceReadPolicy(_$telegramAtom);
    _$telegramAtom.reportObserved();
    return super.telegram;
  }

  @override
  set telegram(String value) {
    _$telegramAtom.context.conditionallyRunInAction(() {
      super.telegram = value;
      _$telegramAtom.reportChanged();
    }, _$telegramAtom, name: '${_$telegramAtom.name}_set');
  }

  final _$imageAtom = Atom(name: '_HomeBase.image');

  @override
  String get image {
    _$imageAtom.context.enforceReadPolicy(_$imageAtom);
    _$imageAtom.reportObserved();
    return super.image;
  }

  @override
  set image(String value) {
    _$imageAtom.context.conditionallyRunInAction(() {
      super.image = value;
      _$imageAtom.reportChanged();
    }, _$imageAtom, name: '${_$imageAtom.name}_set');
  }

  final _$booksNumberAtom = Atom(name: '_HomeBase.booksNumber');

  @override
  String get booksNumber {
    _$booksNumberAtom.context.enforceReadPolicy(_$booksNumberAtom);
    _$booksNumberAtom.reportObserved();
    return super.booksNumber;
  }

  @override
  set booksNumber(String value) {
    _$booksNumberAtom.context.conditionallyRunInAction(() {
      super.booksNumber = value;
      _$booksNumberAtom.reportChanged();
    }, _$booksNumberAtom, name: '${_$booksNumberAtom.name}_set');
  }

  final _$userRatingAtom = Atom(name: '_HomeBase.userRating');

  @override
  String get userRating {
    _$userRatingAtom.context.enforceReadPolicy(_$userRatingAtom);
    _$userRatingAtom.reportObserved();
    return super.userRating;
  }

  @override
  set userRating(String value) {
    _$userRatingAtom.context.conditionallyRunInAction(() {
      super.userRating = value;
      _$userRatingAtom.reportChanged();
    }, _$userRatingAtom, name: '${_$userRatingAtom.name}_set');
  }

  final _$lastSeenAtom = Atom(name: '_HomeBase.lastSeen');

  @override
  String get lastSeen {
    _$lastSeenAtom.context.enforceReadPolicy(_$lastSeenAtom);
    _$lastSeenAtom.reportObserved();
    return super.lastSeen;
  }

  @override
  set lastSeen(String value) {
    _$lastSeenAtom.context.conditionallyRunInAction(() {
      super.lastSeen = value;
      _$lastSeenAtom.reportChanged();
    }, _$lastSeenAtom, name: '${_$lastSeenAtom.name}_set');
  }

  final _$bioAtom = Atom(name: '_HomeBase.bio');

  @override
  String get bio {
    _$bioAtom.context.enforceReadPolicy(_$bioAtom);
    _$bioAtom.reportObserved();
    return super.bio;
  }

  @override
  set bio(String value) {
    _$bioAtom.context.conditionallyRunInAction(() {
      super.bio = value;
      _$bioAtom.reportChanged();
    }, _$bioAtom, name: '${_$bioAtom.name}_set');
  }

  final _$recommendedBooksAtom = Atom(name: '_HomeBase.recommendedBooks');

  @override
  List<Bookshelf> get recommendedBooks {
    _$recommendedBooksAtom.context.enforceReadPolicy(_$recommendedBooksAtom);
    _$recommendedBooksAtom.reportObserved();
    return super.recommendedBooks;
  }

  @override
  set recommendedBooks(List<Bookshelf> value) {
    _$recommendedBooksAtom.context.conditionallyRunInAction(() {
      super.recommendedBooks = value;
      _$recommendedBooksAtom.reportChanged();
    }, _$recommendedBooksAtom, name: '${_$recommendedBooksAtom.name}_set');
  }

  final _$bookshelfBooksAtom = Atom(name: '_HomeBase.bookshelfBooks');

  @override
  List<Bookshelf> get bookshelfBooks {
    _$bookshelfBooksAtom.context.enforceReadPolicy(_$bookshelfBooksAtom);
    _$bookshelfBooksAtom.reportObserved();
    return super.bookshelfBooks;
  }

  @override
  set bookshelfBooks(List<Bookshelf> value) {
    _$bookshelfBooksAtom.context.conditionallyRunInAction(() {
      super.bookshelfBooks = value;
      _$bookshelfBooksAtom.reportChanged();
    }, _$bookshelfBooksAtom, name: '${_$bookshelfBooksAtom.name}_set');
  }

  final _$loadUserInfoAsyncAction = AsyncAction('loadUserInfo');

  @override
  Future<void> loadUserInfo() {
    return _$loadUserInfoAsyncAction.run(() => super.loadUserInfo());
  }

  final _$getUserBookshelfsAsyncAction = AsyncAction('getUserBookshelfs');

  @override
  Future<dynamic> getUserBookshelfs() {
    return _$getUserBookshelfsAsyncAction.run(() => super.getUserBookshelfs());
  }

  @override
  String toString() {
    final string =
        'user: ${user.toString()},name: ${name.toString()},email: ${email.toString()},telegram: ${telegram.toString()},image: ${image.toString()},booksNumber: ${booksNumber.toString()},userRating: ${userRating.toString()},lastSeen: ${lastSeen.toString()},bio: ${bio.toString()},recommendedBooks: ${recommendedBooks.toString()},bookshelfBooks: ${bookshelfBooks.toString()}';
    return '{$string}';
  }
}
