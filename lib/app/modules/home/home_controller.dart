import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/bookshelf_controller.dart';import 'package:tcc_mobile/app/shared/models/bookshelf.dart';
import 'package:tcc_mobile/app/shared/models/sharedPref.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';

part 'home_controller.g.dart';

class HomeController = _HomeBase with _$HomeController;

abstract class _HomeBase with Store {
 
  @observable
  User user = User();

  SharedPref sharedPref = SharedPref();
  
  @action
  Future<void> loadUserInfo() async {
    try{
    await getUserBookshelfs();
    user = User.fromJson(await sharedPref.read('user'));
    name = user.name;
    email = user.email;
    telegram = user.telegram;
    image = user.image;
    bio = user.bio;
    booksNumber =user.numOfBooks.toString();
    userRating = user.evaluation.toString();
    lastSeen = user.lastLogin;
    bookshelfBooks = bookshelfController.bookshelfs;
    } catch(error){
      print(error);
    }
  }
  

  @observable
  String name = "";

  @observable
  String email = "";
  
  @observable
  String telegram;

  @observable
  String image = "";
  
  @observable
  String booksNumber = "";


  @observable 
  String userRating = "";
  

  @observable
  String lastSeen = "";
  
  @observable
  String bio = "";
  
  @observable
  List<Bookshelf> recommendedBooks = [];

  final BookshelfRepositoryController bookshelfController = Modular.get();

  @action
  Future getUserBookshelfs() async{
    try{
    await bookshelfController.getUserBookshelfs();
    } catch(error){
      print(error);
    }
  }


  @observable
  List<Bookshelf> bookshelfBooks = [];


  }