import 'package:tcc_mobile/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/home/home_page.dart';
import 'package:tcc_mobile/app/modules/login/login_controller.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_module.dart';


class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
        Bind((i) => LoginController()),
        Bind((i) => SidemenuModule()),
  ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
