import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/home/components/bookshelfWidget.dart';
import 'package:tcc_mobile/app/modules/home/components/recommendationsWidget.dart';
import 'package:tcc_mobile/app/modules/home/home_controller.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_module.dart';

import 'components/profileWidget.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  @override
  void initState() {
    super.initState();
    controller.loadUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SidemenuModule(),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView(
        children: <Widget>[
          Observer(
              builder: (_) => ProfileWidget(
                    name: controller.name,
                    email: controller.email,
                    telegram: controller.telegram,
                    image: controller.image,
                    bio: controller.bio,
                    userRating: controller.userRating,
                    booksNumber: controller.booksNumber,
                    lastSeen: controller.lastSeen,
                  )),
          SizedBox(height: 15),
          controller.recommendedBooks.isNotEmpty
              ? Column(
                  children: <Widget>[
                    _showSectionDivider('Sugestões', 230.0),
                    Observer(
                      builder: (_) => RecommendationWidget(
                        recommendedBooks: controller.recommendedBooks,

                    )
                    ),
                  ],
                )
              : Container(
                  height: 0,
                ),
          SizedBox(height: 10),
          _showSectionDivider('Minha Estante', 200.0),
          SizedBox(height: 10),
          Observer(
              builder: (_) => BookshelfWidget(
                    bookshelfBooks: controller.bookshelfBooks,
                  )),
        ],
      ),
    );
  }

  Widget _showSectionDivider(String sectionName, [var lineWidth]) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Text(
          sectionName,
          style: TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w300),
        ),
        Container(
          margin: EdgeInsets.fromLTRB(20, 5, 0, 0),
          width: lineWidth,
          height: 0.2,
          color: Colors.black,
        ),
      ],
    );
  }
}
