// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RegisterController on _RegisterBase, Store {
  final _$errorMessageAtom = Atom(name: '_RegisterBase.errorMessage');

  @override
  String get errorMessage {
    _$errorMessageAtom.context.enforceReadPolicy(_$errorMessageAtom);
    _$errorMessageAtom.reportObserved();
    return super.errorMessage;
  }

  @override
  set errorMessage(String value) {
    _$errorMessageAtom.context.conditionallyRunInAction(() {
      super.errorMessage = value;
      _$errorMessageAtom.reportChanged();
    }, _$errorMessageAtom, name: '${_$errorMessageAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_RegisterBase.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_RegisterBase.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_RegisterBase.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$telegramAtom = Atom(name: '_RegisterBase.telegram');

  @override
  String get telegram {
    _$telegramAtom.context.enforceReadPolicy(_$telegramAtom);
    _$telegramAtom.reportObserved();
    return super.telegram;
  }

  @override
  set telegram(String value) {
    _$telegramAtom.context.conditionallyRunInAction(() {
      super.telegram = value;
      _$telegramAtom.reportChanged();
    }, _$telegramAtom, name: '${_$telegramAtom.name}_set');
  }

  final _$registerWithPasswordAsyncAction = AsyncAction('registerWithPassword');

  @override
  Future<dynamic> registerWithPassword() {
    return _$registerWithPasswordAsyncAction
        .run(() => super.registerWithPassword());
  }

  final _$_RegisterBaseActionController =
      ActionController(name: '_RegisterBase');

  @override
  dynamic updateErrorMessage(String value) {
    final _$actionInfo = _$_RegisterBaseActionController.startAction();
    try {
      return super.updateErrorMessage(value);
    } finally {
      _$_RegisterBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeName(String value) {
    final _$actionInfo = _$_RegisterBaseActionController.startAction();
    try {
      return super.changeName(value);
    } finally {
      _$_RegisterBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeEmail(String value) {
    final _$actionInfo = _$_RegisterBaseActionController.startAction();
    try {
      return super.changeEmail(value);
    } finally {
      _$_RegisterBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changePassword(String value) {
    final _$actionInfo = _$_RegisterBaseActionController.startAction();
    try {
      return super.changePassword(value);
    } finally {
      _$_RegisterBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeTelegram(String value) {
    final _$actionInfo = _$_RegisterBaseActionController.startAction();
    try {
      return super.changeTelegram(value);
    } finally {
      _$_RegisterBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'errorMessage: ${errorMessage.toString()},name: ${name.toString()},email: ${email.toString()},password: ${password.toString()},telegram: ${telegram.toString()}';
    return '{$string}';
  }
}
