import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/register_rep_controller.dart';

part 'register_controller.g.dart';

class RegisterController = _RegisterBase with _$RegisterController;

abstract class _RegisterBase with Store {

  @observable
  String errorMessage = ""; 

  @action
  updateErrorMessage(String value) => errorMessage = value;

  @observable
  String name;

  @action
  changeName(String value) => name = value;

  @observable
  String email;

  @action 
  changeEmail(String value) => email = value;

  @observable
  String password;

  @action
  changePassword(String value) => password = value;

  @observable
  String telegram;

  @action
  changeTelegram(String value) => telegram = value;

  final RegisterRepositoryController register = Modular.get();

  @action
  Future registerWithPassword() async{
    try{
    await register.registerWithPassword(name, email, password, telegram);
    
    print(await register.registered);
      if( await register.registered == true)  {
      print('Success!');
      
      Modular.to.pushReplacementNamed('/home');
      } else {
        print('Failed!');
      }

    } on Exception catch(error){
        print(error.toString().substring(11));
        updateErrorMessage(error.toString().substring(11));
    }
   
}


}