import 'package:tcc_mobile/app/modules/login/login_controller.dart';
import 'package:tcc_mobile/app/modules/register/register_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/register/register_page.dart';

class RegisterModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => RegisterController()),
        Bind((i) => LoginController()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => RegisterPage()),
      ];

  static Inject get to => Inject<RegisterModule>.of();
}
