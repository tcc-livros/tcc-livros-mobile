import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/bookshelf_controller.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';

part 'edit_bookshelf_controller.g.dart';

class EditBookshelfController = _EditBookshelfBase
    with _$EditBookshelfController;

abstract class _EditBookshelfBase with Store {
 final BookshelfRepositoryController _bookshelfController = Modular.get();


  @observable
  List<Bookshelf> bookshelfBooks = [];

  @action
  Future loadUserInfo() async{
    try{
      await getUserBookshelfs();
      bookshelfBooks = _bookshelfController.bookshelfs;


    } catch (error){
      print(error);
      throw Exception('Failed loading User Info');
    }
  }


 @action
 Future getUserBookshelfs() async{
   try{
     await _bookshelfController.getUserBookshelfs();
   } catch(error){
     print(error);
      throw Exception('Failed getting User Bookshelves');
   }
 }
}
