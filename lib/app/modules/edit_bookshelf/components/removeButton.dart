import 'package:flutter/material.dart';

class RemoveButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  RawMaterialButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              constraints: BoxConstraints(),
              padding: EdgeInsets.all(6.0), 
              child: Text('Remover', style: TextStyle(fontWeight: FontWeight.bold),), onPressed: () {},
          );
  }
}