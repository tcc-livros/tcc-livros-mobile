import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/components/removeButton.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';

class BookshelfWidget extends StatelessWidget {
  final List<Bookshelf> bookshelfBooks;
  
  const BookshelfWidget({
      Key key,
      @required this.bookshelfBooks,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    
    return Flexible(
      fit: FlexFit.loose,
      child: GridView.builder(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          childAspectRatio: 0.6),
        itemCount: bookshelfBooks.length,
        itemBuilder: (context, index){
          return _showBookShelfCard(bookshelfBooks[index].book.title, bookshelfBooks[index].book.authors.first.name, bookshelfBooks[index].book.imageUrl, context);
        }
      ));
    
  }

  
  Widget _showBookShelfCard(String bookName, String author, String bookImage, BuildContext context){
    return Container(
      child: Column(
        children: <Widget>[
            Container(
                height: 120,
                width: 85,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(bookImage)),
                ),
              ),
            Padding(
                padding: const EdgeInsets.all(0),
                child: Container(
                  child: bookName.length >=13 ? Text(
                    bookName.substring(0,13)+'...', style: TextStyle(fontSize: 15),
                  ) : Text(bookName, style: TextStyle(fontSize: 15))
                ),
              ),
            Container(
              child: author.length <=18 ? Text(
                author, style: TextStyle(fontSize: 10, color: Colors.purple[500]),
              ) :
              Text(
                author.substring(0,17)+'...', style: TextStyle(fontSize: 10, color: Colors.purple[500])),
            ),
            RemoveButton(),
          ],
        ),
    );
  }

}