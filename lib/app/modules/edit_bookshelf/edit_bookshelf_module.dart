import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_page.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_module.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/add_book_search_repository.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/add_book_search_rep_controller.dart';

import 'add_book/add_book_controller.dart';

class EditBookshelfModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => EditBookshelfController()),
         Bind((i) => SidemenuModule()),
         Bind((i) => AddBookController()),
         Bind((i) => AddBookSearchRepositoryController()),
         Bind((i) => AddBookSearchRepository()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => EditBookshelfPage()),
      ];

  static Inject get to => Inject<EditBookshelfModule>.of();
}
