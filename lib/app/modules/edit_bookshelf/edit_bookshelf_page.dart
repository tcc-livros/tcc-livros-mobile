import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/components/bookshelfWidget.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/edit_bookshelf_controller.dart';
import 'package:tcc_mobile/app/modules/sidemenu/sidemenu_module.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';

import 'add_book/components/bookSearch.dart';

class EditBookshelfPage extends StatefulWidget {
  final String title;
  const EditBookshelfPage({Key key, this.title = "EditBookshelf"})
      : super(key: key);

  @override
  _EditBookshelfPageState createState() => _EditBookshelfPageState();
}

class _EditBookshelfPageState extends ModularState<EditBookshelfPage, EditBookshelfController> {

List<Book> searchBooks;

@override
  void initState() {
    controller.loadUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buscar Livro'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search), 
            onPressed: (){
              showSearch(
                context: context, 
                delegate: BookSearch(searchBooks));
            }
            ),
        ],
      ),
      drawer: SidemenuModule(),   
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 10),
          Observer(builder: (_) =>
          controller.bookshelfBooks != null ?  BookshelfWidget(bookshelfBooks: controller.bookshelfBooks,)
           :  Text(
                'Sua Estante está vazia. Adicione um livro!', 
                style: TextStyle(fontSize: 18, 
                  fontWeight: FontWeight.bold, color: Colors.purple)),
          ),
        ],
      ),
    );
  }
}
