// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_bookshelf_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$EditBookshelfController on _EditBookshelfBase, Store {
  final _$bookshelfBooksAtom = Atom(name: '_EditBookshelfBase.bookshelfBooks');

  @override
  List<Bookshelf> get bookshelfBooks {
    _$bookshelfBooksAtom.context.enforceReadPolicy(_$bookshelfBooksAtom);
    _$bookshelfBooksAtom.reportObserved();
    return super.bookshelfBooks;
  }

  @override
  set bookshelfBooks(List<Bookshelf> value) {
    _$bookshelfBooksAtom.context.conditionallyRunInAction(() {
      super.bookshelfBooks = value;
      _$bookshelfBooksAtom.reportChanged();
    }, _$bookshelfBooksAtom, name: '${_$bookshelfBooksAtom.name}_set');
  }

  final _$loadUserInfoAsyncAction = AsyncAction('loadUserInfo');

  @override
  Future<dynamic> loadUserInfo() {
    return _$loadUserInfoAsyncAction.run(() => super.loadUserInfo());
  }

  final _$getUserBookshelfsAsyncAction = AsyncAction('getUserBookshelfs');

  @override
  Future<dynamic> getUserBookshelfs() {
    return _$getUserBookshelfsAsyncAction.run(() => super.getUserBookshelfs());
  }

  @override
  String toString() {
    final string = 'bookshelfBooks: ${bookshelfBooks.toString()}';
    return '{$string}';
  }
}
