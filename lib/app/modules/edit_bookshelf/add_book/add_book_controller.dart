import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repository_controllers/add_book_search_rep_controller.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';

part 'add_book_controller.g.dart';

class AddBookController = _AddBookBase with _$AddBookController;

abstract class _AddBookBase with Store {

  @observable
  List<Book> books;

  @observable
  int qtBooks = 5;

  @observable
  bool requestFailed = false;

  @observable
  String search = 'jogosvorazes';

  final AddBookSearchRepositoryController _searchController = Modular.get();

  @action
  Future getGoogleAPIBooks({String query}) async{
    try{
    await _searchController.getGoogleAPIBooks(query, qtBooks: qtBooks);
    books = _searchController.books;
    } catch(error){
      requestFailed = true;
      throw Exception('Failed getting books from Google API');
    }
  }
  
}
