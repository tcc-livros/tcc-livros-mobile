// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_book_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AddBookController on _AddBookBase, Store {
  final _$booksAtom = Atom(name: '_AddBookBase.books');

  @override
  List<Book> get books {
    _$booksAtom.context.enforceReadPolicy(_$booksAtom);
    _$booksAtom.reportObserved();
    return super.books;
  }

  @override
  set books(List<Book> value) {
    _$booksAtom.context.conditionallyRunInAction(() {
      super.books = value;
      _$booksAtom.reportChanged();
    }, _$booksAtom, name: '${_$booksAtom.name}_set');
  }

  final _$qtBooksAtom = Atom(name: '_AddBookBase.qtBooks');

  @override
  int get qtBooks {
    _$qtBooksAtom.context.enforceReadPolicy(_$qtBooksAtom);
    _$qtBooksAtom.reportObserved();
    return super.qtBooks;
  }

  @override
  set qtBooks(int value) {
    _$qtBooksAtom.context.conditionallyRunInAction(() {
      super.qtBooks = value;
      _$qtBooksAtom.reportChanged();
    }, _$qtBooksAtom, name: '${_$qtBooksAtom.name}_set');
  }

  final _$searchAtom = Atom(name: '_AddBookBase.search');

  @override
  String get search {
    _$searchAtom.context.enforceReadPolicy(_$searchAtom);
    _$searchAtom.reportObserved();
    return super.search;
  }

  @override
  set search(String value) {
    _$searchAtom.context.conditionallyRunInAction(() {
      super.search = value;
      _$searchAtom.reportChanged();
    }, _$searchAtom, name: '${_$searchAtom.name}_set');
  }

  final _$getGoogleAPIBooksAsyncAction = AsyncAction('getGoogleAPIBooks');

  @override
  Future<dynamic> getGoogleAPIBooks({String query}) {
    return _$getGoogleAPIBooksAsyncAction
        .run(() => super.getGoogleAPIBooks(query: query));
  }

  @override
  String toString() {
    final string =
        'books: ${books.toString()},qtBooks: ${qtBooks.toString()},search: ${search.toString()}';
    return '{$string}';
  }
}
