import 'package:flutter/material.dart';

class AddButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  RawMaterialButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              constraints: BoxConstraints(),
              padding: EdgeInsets.all(6.0), 
              child: Text('ADICIONAR', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),), onPressed: () {},
          );
  }
}