//inside search delegate is the class of the widget displayed as a result
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/modules/edit_bookshelf/add_book/add_book_controller.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';

import 'addBookCard.dart';

class BookSearch extends SearchDelegate<Book> {
  final List<Book> books;

  AddBookController controller = AddBookController();
  BookSearch(this.books);

  Future<List<Book>> getResults() async {
    await controller.getGoogleAPIBooks(
        query: query.toLowerCase().replaceAll(' ', ''));
    return controller.books;
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            close(context, null);
            controller.books.clear();
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          close(context, null);
          controller.books.clear();
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return Observer(
      builder: (context) => FutureBuilder(
          future: getResults(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasData) {
                return ListView.builder(
                    padding: EdgeInsets.only(bottom: 10, top: 10),
                    itemCount: controller.books.length,
                    itemBuilder: (BuildContext context, int index) {
                      return AddBookCard(book: controller.books[index]);
                    });
              } else {
                return Center(
                  child: Text('Livro não encontrado!', style: TextStyle(fontSize: 18),),
                );
              }
            }
            return Center(child: CircularProgressIndicator(),);
          }),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return Column();
  }
}
