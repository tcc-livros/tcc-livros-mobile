import 'package:flutter/material.dart';
import 'package:tcc_mobile/app/shared/models/authors.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';

import 'addButton.dart';

class AddBookCard extends StatelessWidget {
  final Book book;
  Authors _authors = Authors();

  AddBookCard({
    Key key,
    @required this.book,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _showAddBookCard(book.title, book.authors.firstWhere((Authors authors)=>book.authors.first != null, orElse: ()=>_authors ).name, book.imageUrl);
  }

  Widget _showAddBookCard(String bookName, String author, String bookImage) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: 85.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: bookImage != null
                      ? NetworkImage(bookImage)
                      : AssetImage('assets/images/bookSample.png')),
            ),
          ),
          Container(
              child: bookName.length <= 17
                  ? Text(
                      bookName,
                      style: TextStyle(fontSize: 18),
                    )
                  : Text(
                      bookName.substring(0, 15) + '...',
                      style: TextStyle(fontSize: 18),
                    )),
          author != null ?
          Container(
            child: author.length <= 18
                ? Text(
                    author,
                    style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                  )
                : Text(
                    author.substring(0, 15) + '...',
                    style: TextStyle(fontSize: 15, color: Colors.purple[500]),
                  ),
          ) :
          Container(
            child: Text('Sem autor',style: TextStyle(fontSize: 15, color: Colors.purple[500]), ) 
          ),
          AddButton(),
        ],
      ),
    );
  }
}
