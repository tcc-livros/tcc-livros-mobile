
import 'bookshelf.dart';

class User {
  String name;
  String telegram;
  String email;
  int numOfBooks;
  double evaluation;
  String bio;
  String image;
  String createdAt;
  String lastLogin;
  String token;
  List<Bookshelf> bookshelfs = List<Bookshelf>();


  User(
      {this.name,
      this.telegram,
      this.email,
      this.numOfBooks,
      this.evaluation,
      this.bio,
      this.image,
      this.createdAt,
      this.lastLogin,
      this.token});




  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    telegram = json['telegram'];
    email = json['email'];
    numOfBooks = json['num_of_books'];
    evaluation = json['evaluation'];
    bio = json['bio'];
    image = json['image'];
    createdAt = json['created_at'];
    lastLogin = json['last_login'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['telegram'] = this.telegram;
    data['email'] = this.email;
    data['num_of_books'] = this.numOfBooks;
    data['evaluation'] = this.evaluation;
    data['bio'] = this.bio;
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['last_login'] = this.lastLogin;
    data['token'] = this.token;
    return data;
  }
  
  String toString(){
    return 'Usuario(name: $name, telegram: $telegram, email: $email, num of books: $numOfBooks, evaluation: $evaluation, bio: $bio, image: $image, created_at: $createdAt, last_login: $lastLogin, token: $token)';
  }

}


 