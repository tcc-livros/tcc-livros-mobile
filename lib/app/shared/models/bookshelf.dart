import 'book.dart';

class Bookshelf {
  int owner;
  Book book;
  var evaluation;
  bool available;

  Bookshelf({this.owner, this.book, this.evaluation, this.available});

  Bookshelf.fromJson(Map<String, dynamic> json) {
    owner = json['owner'];
    book = json['book'] != null ? new Book.fromJson(json['book']) : null;
    evaluation = json['evaluation'];
    available = json['available'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['owner'] = this.owner;
    if (this.book != null) {
      data['book'] = this.book.toJson();
    }
    data['evaluation'] = this.evaluation;
    data['available'] = this.available;
    return data;
  }
}