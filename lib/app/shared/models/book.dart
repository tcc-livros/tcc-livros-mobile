import 'authors.dart';
import 'categories.dart';

class Book {
  String title;
  List<Authors> authors = new List<Authors>();
  String imageUrl;
  List<Categories> categories = new List<Categories>();
  String isbn_10;
  String isbn_13;

  Book({this.title, this.authors, this.imageUrl, this.categories, this.isbn_10});

  Book.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    if (json['authors'] != null) {
      authors = new List<Authors>();
      json['authors'].forEach((v) {
        authors.add(new Authors.fromJson(v));
      });
    }
    imageUrl = json['image_url'];
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    isbn_10 = json["ISBN_10"];
    isbn_13 = json["ISBN_13"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    if (this.authors != null) {
      data['authors'] = this.authors.map((v) => v.toJson()).toList();
    }
    data['image_url'] = this.imageUrl;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['ISBN_10'] = this.isbn_10;
    data['ISBN_13'] = this.isbn_13;
    return data;
  }
}
