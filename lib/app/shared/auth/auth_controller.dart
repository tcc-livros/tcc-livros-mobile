import 'dart:async';

import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/auth/repositories/auth_repository.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';
part 'auth_controller.g.dart';

class AuthController = _AuthControllerBase with _$AuthController;

abstract class _AuthControllerBase with Store {
  final AuthRepository _authRepository = Modular.get();

  @observable
  Future<User> user;

  Future loginWithPassword(String email, String password) async {
    user = _authRepository.getEmailPasswordLogin(email, password);
  }
  
  void logOut(){
    _authRepository.getLogout();
  }


}