import 'package:tcc_mobile/app/shared/models/user.dart';

abstract class IAuthRepository{
  Future<User> getEmailPasswordLogin(String email, String password);
  void getLogout(); 
  Future<String> getToken();
}