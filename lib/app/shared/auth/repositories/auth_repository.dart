import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:tcc_mobile/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:tcc_mobile/app/shared/models/sharedPref.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';

class AuthRepository implements IAuthRepository {

  final Dio dio;
  AuthRepository(this.dio);
  SharedPref sharedPref = SharedPref();

  @override
  Future<User> getEmailPasswordLogin(String email, String password) async {
    User user;

    

    try{
    Map params = {
      'email' : email,
      'password' : password,
    };

    var _body = json.encode(params);
    var response = await dio.post('accounts/login/', data: _body);

    
    if(response.statusCode == 200 || response.statusCode == 201){
      user = User.fromJson(response.data['user']);
      sharedPref.save('user',  user);
      return user;
    }else if(response.statusCode == 400){
      throw Exception("email ou senha incorretos");
    }else throw Exception('erro de autentificação');
  } on DioError catch(exception){
      if(exception == null || exception.toString().contains('SocketException')){
        throw Exception('erro de rede');
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT){
            throw Exception("Não foi possível conectar. Verifique sua conexão com a internet");
          }else{
            return user;
          }

  }

    
    
    
  
    
  }

  @override
  void getLogout() async {
    await sharedPref.clear();   
  }

  @override
  Future<String> getToken() {
    // TODO: implement getToken
    return null;
  }

}