import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/add_book_search_repository.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';
part 'add_book_search_rep_controller.g.dart';

class AddBookSearchRepositoryController = _AddBookSearchRepositoryControllerBase with _$AddBookSearchRepositoryController;

abstract class _AddBookSearchRepositoryControllerBase with Store{
  final AddBookSearchRepository _addBookSearchRepository = Modular.get();

  @observable
  List<Book> books;

  Future getGoogleAPIBooks(String search, {int qtBooks}) async{
    books = await _addBookSearchRepository.fetchBooks(search, qtBooks: qtBooks);
  }


}