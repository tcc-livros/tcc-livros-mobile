// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bookshelf_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BookshelfRepositoryController
    on _BookshelfRepositoryControllerBase, Store {
  final _$bookshelfsAtom =
      Atom(name: '_BookshelfRepositoryControllerBase.bookshelfs');

  @override
  List<Bookshelf> get bookshelfs {
    _$bookshelfsAtom.context.enforceReadPolicy(_$bookshelfsAtom);
    _$bookshelfsAtom.reportObserved();
    return super.bookshelfs;
  }

  @override
  set bookshelfs(List<Bookshelf> value) {
    _$bookshelfsAtom.context.conditionallyRunInAction(() {
      super.bookshelfs = value;
      _$bookshelfsAtom.reportChanged();
    }, _$bookshelfsAtom, name: '${_$bookshelfsAtom.name}_set');
  }

  @override
  String toString() {
    final string = 'bookshelfs: ${bookshelfs.toString()}';
    return '{$string}';
  }
}
