import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/register_repository.dart';
part 'register_rep_controller.g.dart';

class RegisterRepositoryController = _RegisterRepositoryControllerBase with _$RegisterRepositoryController;

abstract class _RegisterRepositoryControllerBase with Store {
  final RegisterRepository _registerRepository = Modular.get();

  @observable
  Future<bool> registered;

  
  Future registerWithPassword(String name, String email, String password, String telegram) async {
    registered = _registerRepository.getRegisterWithPassword(name, email, password, telegram);
  }
}