import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:tcc_mobile/app/shared/connections/repositories/bookshelf_repository.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';

part 'bookshelf_controller.g.dart';

class BookshelfRepositoryController = _BookshelfRepositoryControllerBase with _$BookshelfRepositoryController;

abstract class _BookshelfRepositoryControllerBase with Store {
  final BookshelfRepository _bookshelfRepository = Modular.get();

  @observable
  List<Bookshelf> bookshelfs;
  
  Future getUserBookshelfs() async{
     bookshelfs = await _bookshelfRepository.getUserBookshelfs();
  }

}