// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_rep_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RegisterRepositoryController
    on _RegisterRepositoryControllerBase, Store {
  final _$registeredAtom =
      Atom(name: '_RegisterRepositoryControllerBase.registered');

  @override
  Future<bool> get registered {
    _$registeredAtom.context.enforceReadPolicy(_$registeredAtom);
    _$registeredAtom.reportObserved();
    return super.registered;
  }

  @override
  set registered(Future<bool> value) {
    _$registeredAtom.context.conditionallyRunInAction(() {
      super.registered = value;
      _$registeredAtom.reportChanged();
    }, _$registeredAtom, name: '${_$registeredAtom.name}_set');
  }

  @override
  String toString() {
    final string = 'registered: ${registered.toString()}';
    return '{$string}';
  }
}
