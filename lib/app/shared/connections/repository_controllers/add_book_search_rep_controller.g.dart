// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_book_search_rep_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AddBookSearchRepositoryController
    on _AddBookSearchRepositoryControllerBase, Store {
  final _$booksAtom =
      Atom(name: '_AddBookSearchRepositoryControllerBase.books');

  @override
  List<Book> get books {
    _$booksAtom.context.enforceReadPolicy(_$booksAtom);
    _$booksAtom.reportObserved();
    return super.books;
  }

  @override
  set books(List<Book> value) {
    _$booksAtom.context.conditionallyRunInAction(() {
      super.books = value;
      _$booksAtom.reportChanged();
    }, _$booksAtom, name: '${_$booksAtom.name}_set');
  }

  @override
  String toString() {
    final string = 'books: ${books.toString()}';
    return '{$string}';
  }
}
