import 'package:dio/dio.dart';
import 'package:tcc_mobile/app/shared/models/bookshelf.dart';
import 'package:tcc_mobile/app/shared/models/sharedPref.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';

class BookshelfRepository{
  final Dio dio;

  BookshelfRepository(this.dio);
  
  Future<List<Bookshelf>> getUserBookshelfs() async {
    Response response;
    List<Bookshelf> bookshelfs = List<Bookshelf>();
    SharedPref sharedPref = SharedPref();

    try{

      User user = User.fromJson(await sharedPref.read('user'));

      var header = {
      "Content-Type": "application/json",
      "Authorization": "token ${user.token}",
      };
    
    response = await dio.get('bookshelf/', options: Options(headers: header),);

    if(response.statusCode == 200){
      for(Map map in response.data){
        Bookshelf bookshelf = Bookshelf.fromJson(map);
        bookshelfs.add(bookshelf);
      }
      user.bookshelfs = bookshelfs;
      sharedPref.save('user', user);
      return bookshelfs;
    }else{
      throw Exception('Problema na requisição');
    }

    } on DioError catch(exception){
       if(exception == null || exception.toString().contains('SocketException')){
        throw Exception('erro de rede');
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT){
            throw Exception("Não foi possível conectar. Verifique sua conexão com a internet");
          }else{
            return bookshelfs;
          }
    }
    
    

  }

}