import 'package:dio/dio.dart';
import 'package:tcc_mobile/app/shared/models/authors.dart';
import 'package:tcc_mobile/app/shared/models/book.dart';

class AddBookSearchRepository {
  Dio dio = Dio();
  List<Book> books = new List<Book>();
  final _url = "https://www.googleapis.com/books/v1/volumes?q=";

  Future<List<Book>> fetchBooks(String search, {int qtBooks}) async {
    try {
      var response = await dio.get(_url + search);
      if (response.statusCode == 200) {
        print(response.data);
        print(response.data.runtimeType);
        var qt = (qtBooks == null) ? 1 : qtBooks;

        for (int index = 0; index < qt; index++) {
          Book book = new Book();
          book.authors = [];
          var firstItem = response.data['items'][index];
          var volumeInfo = firstItem['volumeInfo'];
          book.title = volumeInfo['title'];
          if (volumeInfo.containsKey('authors')) {
            book.authors.add(Authors(name: volumeInfo['authors'][0]));
          }
          if (volumeInfo['industryIdentifiers'][0]['type'] == 'ISBN_13') {
            book.isbn_13 = volumeInfo['industryIdentifiers'][0]['identifier'];
          }
          if (volumeInfo['industryIdentifiers'][0]['type'] == 'ISBN_10') {
            book.isbn_10 = volumeInfo['industryIdentifiers'][0]['identifier'];
          }
          if (volumeInfo.containsKey('imageLinks')) {
            book.imageUrl = volumeInfo['imageLinks']['smallThumbnail'];
          }
          books.add(book);
        }
        return books;
      } 
      else if(response.statusCode == 404){
        throw Exception('Livro não encontrado na API');
      }      
      else {
        throw Exception(
            "Requisição falhou com status: ${response.statusCode}.");
      }
    } on DioError catch (exception) {
      if (exception == null ||
          exception.toString().contains('SocketException')) {
        throw Exception('erro de rede');
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT) {
        throw Exception(
            "Não foi possível conectar. Verifique sua conexão com a internet");
      } else {
        return books;
      }
    }
  }
}
