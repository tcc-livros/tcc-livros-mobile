import 'dart:convert';

import 'package:dio/dio.dart';

import 'package:tcc_mobile/app/shared/connections/repositories/register_repository_interface.dart';
import 'package:tcc_mobile/app/shared/models/sharedPref.dart';
import 'package:tcc_mobile/app/shared/models/user.dart';

class RegisterRepository implements IRegisterRepository{
  final Dio dio;
  RegisterRepository(this.dio);

  
  @override
  Future<bool> getRegisterWithPassword(String name, String email, String password, String telegram) async {
    bool registered = false;
    Response response;
    Map params;
    User user;
    SharedPref sharedPrefs = SharedPref();

    
    try{

      if(telegram == null){
        params = {
        'name': name,
        'email': email,
        'password': password,
        };
      } else{
        params = {
        'name': name,
        'email': email,
        'password': password,
        'telegram': telegram,
        };
      }
    
    
    var _body = json.encode(params);
  
    response = await dio.post('accounts/register/', data: _body, options: Options(
        followRedirects: false,
        validateStatus: (status) { return status < 500; }
    ),);

    if(response.statusCode == 200 || response.statusCode == 201){
      registered = true;
      user = User.fromJson(response.data['user']);
      sharedPrefs.save('user', user);
      return registered;
    } 
    else if(response.statusCode == 409){
      throw Exception('${response.data['message']}');
    } 
    } on DioError catch(exception){
      if(exception == null || exception.toString().contains('SocketException')){
        throw Exception('erro de rede');
      } else if (exception.type == DioErrorType.RECEIVE_TIMEOUT ||
          exception.type == DioErrorType.CONNECT_TIMEOUT){
            throw Exception("Não foi possível conectar. Verifique sua conexão com a internet");
          }
  }
    return registered;
  }
}