abstract class IRegisterRepository{
  Future<bool> getRegisterWithPassword(String name, String email, String password, String telegram); 
}